module.exports = function (db,app) {
    db.getConnection(function (err, db) {

        if (err) {

            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            }
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            }
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            } else {
                console.error('Unable to connect to the database:');
                console.error(err)
                console.log("Shutdown...")
                process.exit()

            }
            db.on('error', function (err) {
                console.log(err)
                throw err;
                return;
            });
        } else {
            console.log('Connection has been established successfully.')

            db.query(`show status like 'questions';`,
                function (error, data, fields) {
                    console.log(error);
                    if (!error) {
                        console.log('Total Questions =' + data[0].Value)
                    } else {
                        console.log(error)
                    }
                })


            app.listen(process.env.PORT || 3000)

        }
        db.release();
        console.log('released')
    });
}