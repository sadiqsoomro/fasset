# FASSET TASK
.env file content

DB_HOST = 148.66.136.123
DB = fasset_task
DB_USER = fasset
DB_PASSWORD = Budwf75qvE5hAkq5
DB_CONNECTIONS = 50

# Database is hosted on server, you can connect to mysql using mysql workbench by using credentials mentioned above.
# API is hosted on heroku on following URL: https://fasset.herokuapp.com
# Documentation is hosted on swagger hub on following URL: https://app.swaggerhub.com/apis-docs/sadiq3/Fasset/1.0.0-oas3#/

API returns response in specific format which contains metadata defining the nature of the response being either error or success.
In case of error, further information is accessible.

Password and user name policy validation is skipped.
JWT secret is hardcoded.

#Test cases are generated to validate password encryption algorithm

Test case results:
 PASS  __tests__/password_algorithm.js
   TST-LCA-04::[Password-Verifying-Algo] Check password verification algorithim NOTE: THESE TEST CASES COVERS TESTING OF THE PASSWORD VALIDATIN
   ALGORITHIM ONLY
   
   √ 001: Validated password algo using the existing key: Success Case (3 ms)
   √ 002: Validated password algo using the invalid encryption key: Failure Case(2ms)
   √ 003: Validated password algo using the invalid Empty provided password string: Failure Case
   √ 004: Validated password algo using the clear text hashed string password string: Failure Case (1 ms) 
                                                                                                                                                    
    Test Suites: 1 passed, 1           
    total 
    Tests:       4 passed, 4
    total                                                                                                                                 
    Snapshots:   0 total
    Time:        0.609 s, estimated 1 s
    Ran all test suites.
