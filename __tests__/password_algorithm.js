const PasswordUtil = require('../utils/utils')
var cryptoAES = require("crypto-js");
const crypto = require('crypto');
const argon2 = require('argon2');
describe("TST-LCA-04::[Password-Verifying-Algo] Check password verification algorithim \nNOTE: THESE TEST CASES COVERS TESTING OF THE PASSWORD VALIDATIN ALGORITHIM ONLY", () => {
    
    test(`001: Validated password algo using the existing key: Success Case`, async () => {
        try {
            process.env.ENCRYPTION_KEY = 'Thequickbrownfoxjumpedoverthelazydog';
            const isVerified = PasswordUtil.verify('judjment', 'U2FsdGVkX1+6RnGbHw9cw9XY8iT/WhhEPUYqkWo7xMrksRTOuLDtszp4lZKKUQ3c+EC9KwxNv/W0/z7S+ksv0jWrqnzao7brYOB2nbH1DBUbClKArkqdsbmgn/a9sdLZT2SsZ4IGqCGUxnH2cNOU3JBbfLdnjEGGchpFLYzarfWXIGzKUfgQhGBw2GJYH7+l');
            expect(isVerified).toBeTruthy();
        } catch (error) {
            fail(error);
        }
    });

    test(`002: Validated password algo using the invalid encryption key: Failure Case`, async () => {
        
        process.env.ENCRYPTION_KEY = 'asdfasdfasdfasdfwrzdxvzxcvaerqwerasd';
        try {
            const isVerified = PasswordUtil.verify('judjment', 'U2FsdGVkX1+6RnGbHw9cw9XY8iT/WhhEPUYqkWo7xMrksRTOuLDtszp4lZKKUQ3c+EC9KwxNv/W0/z7S+ksv0jWrqnzao7brYOB2nbH1DBUbClKArkqdsbmgn/a9sdLZT2SsZ4IGqCGUxnH2cNOU3JBbfLdnjEGGchpFLYzarfWXIGzKUfgQhGBw2GJYH7+l');
            expect(isVerified).not.toBeTruthy();
        } catch (error) {
            expect(true).toBeTruthy();
        }
    });

    test(`003: Validated password algo using the invalid Empty provided password string: Failure Case`, async () => {
        
        process.env.ENCRYPTION_KEY  = 'Thequickbrownfoxjumpedoverthelazydog';
        try {
            const isVerified = PasswordUtil.verify('', 'U2FsdGVkX1+6RnGbHw9cw9XY8iT/WhhEPUYqkWo7xMrksRTOuLDtszp4lZKKUQ3c+EC9KwxNv/W0/z7S+ksv0jWrqnzao7brYOB2nbH1DBUbClKArkqdsbmgn/a9sdLZT2SsZ4IGqCGUxnH2cNOU3JBbfLdnjEGGchpFLYzarfWXIGzKUfgQhGBw2GJYH7+l');
            expect(isVerified).not.toBeTruthy();
        } catch (error) {
            expect(true).toBeTruthy();
        }
    });

    test(`004: Validated password algo using the clear text hashed string password string: Failure Case`, async () => {
        
        process.env.ENCRYPTION_KEY  = 'Thequickbrownfoxjumpedoverthelazydog';
        try {
            const isVerified = PasswordUtil.verify('judjment', 'judjment');
            expect(isVerified).not.toBeTruthy();            
        } catch (error) {            
            expect(true).toBeTruthy();
        }
    });
});