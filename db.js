

if (!global.hasOwnProperty('db')) {

  const Sequelize = require("sequelize");

  let CONNECTION_POOL = parseInt(process.env.DB_CONNECTIONS);

  console.log('Connection Pool Size:' + CONNECTION_POOL)
  if (global.db) {
    global.db = global.db;
  } else {
    var opts = {
      define: {
        freezeTableName: true
      }
    }
    const sequalize = new Sequelize(process.env.DB, process.env.DB_USER, process.env.DB_PASSWORD, {
      host: process.env.DB_HOST,
      dialect: 'mysql',
      port: 3306,
      pool: {
        max: CONNECTION_POOL,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    }, opts);
    global.db = sequalize;
  }
}
module.exports = global.db
