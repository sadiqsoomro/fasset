require('dotenv').config();
require('events').EventEmitter.prototype._maxListeners = 100;
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./api/v1/routes/routes-index');
const cors = require('cors');
const db = require('./db');
(async () => {
     await db.sync();
})();
const app = express();
app.use(bodyParser.json({
    limit: '10mb',
    extended: true
}))

app.use(cors())

routes(app)
app.listen(process.env.PORT || 3000)