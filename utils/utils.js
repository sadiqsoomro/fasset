const uuid = require('uuid');
var cryptoAES = require("crypto-js");
const crypto = require('crypto');
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');



const _CONFIGURATION = {
  memoryCost: 15360,
  parallelism : 1
}

module.exports.UUID = () => {
    return uuid.v4().toString().toUpperCase().replace(/-/g, "")
}

module.exports.RESPONSE_JSON = ()=> {
    return {
      "metadata": {
        "status": "0000",
        "description": "Success",
        "responseTime": new Date().toISOString()
      }
    }
}

module.exports.EncryptPassword = async (plainPassword) => {
  const _EncKey = process.env.PASSWORD_ENC_KEY;
  const salt = crypto.randomBytes(16).toString('base64');
  const saltFirst8 = salt.substr(0,8);
  const saltLast8 = salt.substr(salt.length-8);
  const saltedPassword = saltFirst8 + plainPassword + saltLast8;
  const argonHash = await argon2.hash(saltedPassword,_CONFIGURATION);
  const saltedArgonHash = saltFirst8+argonHash+saltLast8;
  const aesEncrypted = cryptoAES.AES.encrypt(saltedArgonHash, _EncKey).toString()
  return aesEncrypted;
} 

module.exports.verify = async(inputPassword,hashedPassword) => {
  const _EncKey = process.env.PASSWORD_ENC_KEY;

  let passLenthWoPep = 0;
  const bytes = cryptoAES.AES.decrypt(hashedPassword, _EncKey)
  const argonWithSalt = bytes.toString(cryptoAES.enc.Utf8);
  passLenthWoPep = argonWithSalt.length;
  const saltFirst8 = argonWithSalt.substr(0,8);
      const saltLast8 = argonWithSalt.substr(argonWithSalt.length-8,argonWithSalt.length);
      const argonWithoutSalt = argonWithSalt.substr(8,argonWithSalt.length-16);
      const saltedPassword = saltFirst8+inputPassword+saltLast8
      const verification = await argon2.verify(argonWithoutSalt,saltedPassword,this._CONFIGURATION);
      return verification;     
}

module.exports.createJWT = async(userUUID) =>{
  const JWT_TOKEN = await jwt.sign({USER_ID: userUUID}, "aquickbrownfoxjumpsoveralazydog")
  return JWT_TOKEN;
}
