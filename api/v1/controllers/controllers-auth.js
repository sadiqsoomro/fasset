
const    jwt = require('jsonwebtoken');
exports.validateRequestToken = function (req, res, next) {
    try {
        let decodedToken = jwt.decode(req.headers.authorization.split(' ')[1])
        let AuthToken = req.headers.authorization.split(' ')[1]
        const apiKey = decodedToken.apiKey
        const sourceUuid = decodedToken.sourceUuid

        // -------
        jwt.verify(AuthToken, appConfig.JWT_SECRET_KEY, function (err, decoded) {
            if (!err) {
                next()
            } else {
                if (!res.finished) res.status(511).json({ response: new Error("invalid token") })
            }
        })
    } catch (error) {
        console.error(req.url + ': bearer token not in header')
        if (!res.finished) res.status(401).json({ response: new Error("invalid token", 'bearer token not in header', error.toString()) })
    }
}