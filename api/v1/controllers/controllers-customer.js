const modal_customer = require("../models/model-customer")();
const modal_user = require("../models/model-user")();

const utils = require("../../../utils/utils")

exports.createCustomer = async (req, res) => {
  const { NAME, EMAIL, PHONE_NUM } = req.body
  const UUID = utils.UUID();
  const {USER_ID} = res.locals.USER_DATA;
  const responseJSON = utils.RESPONSE_JSON();
  try {
    await modal_customer.create({
      UUID: UUID, NAME: NAME, EMAIL: EMAIL, PHONE_NUM: PHONE_NUM, CREATED_BY: USER_ID
    })
    res.status(200).send(responseJSON);
  } catch (err) {
    responseJSON.metadata.status = "5000"
    responseJSON.metadata.description = "Error";
    responseJSON.errorDetails = err.message;
    res.status(500).send(responseJSON);
  }
}

exports.getCustomer = async (req, res) => {

  const responseJSON = utils.RESPONSE_JSON();
  const userId = res.locals.USER_ID;

  try {
    const resp = await modal_customer.findAll({attributes: ['UUID','NAME', 'EMAIL','PHONE_NUM']},{include: [{
      model: modal_user,
      WHERE: {CREATED_BY: userId},
      required: true
     }]})

    responseJSON.customerInfo = resp;
    res.status(200).send(responseJSON);
  } catch (err) {
    responseJSON.metadata.status = "5000"
    responseJSON.metadata.description = "Error";
    responseJSON.errorDetails = err.message;
    res.status(500).send(responseJSON);
  }
}

exports.deleteCustomer = async (req, res) => {

  const responseJSON = utils.RESPONSE_JSON();
  try {
    const CUSTOMER_ID = req.params.CUSTOMER_ID;
    const resp = await modal_customer.destroy({where: {UUID: CUSTOMER_ID}})
    let status = 200;
    if(resp == 0){
      status = 404;
      responseJSON.metadata.status = "4004"
      responseJSON.metadata.description = "Error";
      responseJSON.errorDetails = "Customer not found.";
    }
    res.status(status).send(responseJSON);
  } catch (err) {
    responseJSON.metadata.status = "5000"
    responseJSON.metadata.description = "Error";
    responseJSON.errorDetails = err.message;
    res.status(500).send(responseJSON);
  }
}