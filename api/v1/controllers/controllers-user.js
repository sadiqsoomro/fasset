const modal_user = require("../models/model-user")();
const utils = require("../../../utils/utils")

exports.registerUser = async (req, res) => {
  const { USER_NAME, PASSWORD, IS_ADMIN } = req.body
  const enrcyptedPassword = await utils.EncryptPassword(PASSWORD);
  const IS_ACTIVE = IS_ADMIN;
  const UUID = utils.UUID();

  const [user, created] = await modal_user.findOrCreate({
    where: { USER_NAME: USER_NAME },
    defaults: { UUID: UUID, USER_NAME: USER_NAME, PASSWORD: enrcyptedPassword, IS_ADMIN: IS_ADMIN, IS_ACTIVE: IS_ACTIVE }
  })
  const responseJSON = utils.RESPONSE_JSON();
  let status = 200;
  if (!created) {
    status = 500;
    responseJSON.metadata.status = "4002"
    responseJSON.metadata.description = "Error";
    responseJSON.errorDetails = "User already exists.";
  } else responseJSON.USER_ID = UUID;

  res.status(status).send(responseJSON);
}

exports.loginUser = async (req, res) => {
  const { USER_NAME, PASSWORD } = req.body
  const responseJSON = utils.RESPONSE_JSON();
  try {
    const resp = await modal_user.findOne({ where: { USER_NAME: USER_NAME } })
    let status = 200;
    const respData = resp == null? null : resp.dataValues;
    if (resp == null || resp.uniqno == 0) {
      status = 401;
      responseJSON.metadata.status = "4001"
      responseJSON.metadata.description = "Error";
      responseJSON.errorDetails = "Invalid username or password.";
    } else if (respData.IS_ACTIVE == 0) {
      status = 403;
      responseJSON.metadata.status = "4003"
      responseJSON.metadata.description = "Error";
      responseJSON.errorDetails = "User locked, contact administrator.";
    } else {
      const isPasswordValid = await utils.verify(PASSWORD, respData.PASSWORD);
      if (isPasswordValid) {
        const JWT_TOKEN =  await utils.createJWT({USER_ID: respData.UUID, IS_ADMIN:respData.IS_ADMIN});
        responseJSON.login_session = { token: JWT_TOKEN, expiry: 3600 };
      } else {
        status = 401;
        responseJSON.metadata.status = "4001"
        responseJSON.metadata.description = "Error";
        responseJSON.errorDetails = "Invalid username or password.";
      }
    }
    res.status(status).send(responseJSON);
  } catch (err) {
    responseJSON.metadata.status = "5000"
    responseJSON.metadata.description = "Error";
    responseJSON.errorDetails = err.message;
    res.status(500).send(responseJSON);
  }
}

exports.updateRole = async (req, res) => {
  const { USER_ID, ROLE_TYPE } = req.body
  const responseJSON = utils.RESPONSE_JSON();
  try {
    const resp = await modal_user.update(
      { IS_ADMIN: ROLE_TYPE },
      { where: { UUID: USER_ID} }
    )
    let status = 200;
    const respData = resp.dataValues;
    if (resp.uniqno == 0) {
      status = 401;
      responseJSON.metadata.status = "4001"
      responseJSON.metadata.description = "Error";
      responseJSON.errorDetails = "Invalid username or password.";
    } else if (respData.IS_ACTIVE == 0) {
      status = 403;
      responseJSON.metadata.status = "4003"
      responseJSON.metadata.description = "Error";
      responseJSON.errorDetails = "User locked, contact administrator.";
    } else {
      const isPasswordValid = await utils.verify(PASSWORD, respData.PASSWORD);
      if (isPasswordValid) {
        const JWT_TOKEN =  await utils.createJWT(respData.UUID);
        responseJSON.login_session = { token: JWT_TOKEN, expiry: 3600 };
      } else {
        status = 401;
        responseJSON.metadata.status = "4001"
        responseJSON.metadata.description = "Error";
        responseJSON.errorDetails = "Invalid username or password.";
      }
    }
    res.status(status).send(responseJSON);
  } catch (err) {
    responseJSON.metadata.status = "5000"
    responseJSON.metadata.description = "Error";
    responseJSON.errorDetails = err.message;
    res.status(500).send(responseJSON);
  }
}

exports.activateUser = async (req, res) => {
  const { USER_ID } = req.params
  const responseJSON = utils.RESPONSE_JSON();
  let status = 200;

  try {
    const resp = await modal_user.update(
      { IS_ACTIVE: 1 },
      { where: {UUID: USER_ID} }
    )
    if (resp[0] == 0) {
      status = 404;
      responseJSON.metadata.status = "4004"
      responseJSON.metadata.description = "Error";
      responseJSON.errorDetails = "User not found.";
    }
  } catch (err) {
    status = 500
    responseJSON.metadata.status = "5000"
    responseJSON.metadata.description = "Error";
    responseJSON.errorDetails = err.message;
  }
  res.status(status).send(responseJSON);

}

//activateUser