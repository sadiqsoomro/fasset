module.exports = function (app) {
    require('./routes-auth')(app),
    require('./routes-user')(app),
    require('./routes-customer')(app),
    invalidRoute = require('./routes-invalid-route.js')(app)
}
