const controller_users = require('../controllers/controllers-user')
const schema_users = require('../schemas/schema-user')
const expressJoiMiddleware = require('express-joi-middleware');
const roleMiddleware = require('../middlewares/role-middleware');
const sessionMiddleware = require('../middlewares/session-middleware');

module.exports = function (app) {
    app.post('/user/register',expressJoiMiddleware(schema_users.userValidation), function (req, res) {
        controller_users.registerUser(req, res)
    })

    app.post('/user/login',expressJoiMiddleware(schema_users.loginValidation), function (req, res) {
        controller_users.loginUser(req, res)
    })

    app.put('/user/role',expressJoiMiddleware(schema_users.updateRoleValidation),roleMiddleware, function (req, res) {
        controller_users.updateRole(req, res)
    })

    app.put('/user/activate/:USER_ID',expressJoiMiddleware(schema_users.activateUserValidation),sessionMiddleware,roleMiddleware, function (req, res) {
        controller_users.activateUser(req, res)
    })
}
