module.exports = function (app) {
    app.use( (req, res, next) => {
        res.status(405).json({
            "metadata": {
                "status": "9999",
                "description": "Error",
                "responseTime": new Date().toISOString()
            },
            "errorDetails" : "Invalid URL."
        });   
    });   

    app.use((err, req, res, next) => {
        if (err.isJoi) {
            res.status(400).json({
                "metadata": {
                    "status": "8001",
                    "description": "Error",
                    "responseTime": new Date().toISOString()
                },
                "errorDetails" : err.details[0].message
            });
            return;
        }
        
        res.status(500).send({
            "metadata": {
                "status": "9000",
                "description": "Error",
                "responseTime": new Date().toISOString()
            },
            "errorDetails" : err.message
        });
    });
}