module.exports = function (app) {
    const controller = require('../controllers/controllers-customer')
    const schema = require('../schemas/schema-customer')
    const expressJoiMiddleware = require('express-joi-middleware');
    const sessionMiddleware = require("../middlewares/session-middleware")
    
    app.post('/customer',expressJoiMiddleware(schema.createCustomerValidation),sessionMiddleware, function (req, res) {
        controller.createCustomer(req, res)
    })

    app.get('/customer',expressJoiMiddleware(schema.getCustomerValidation),sessionMiddleware, function (req, res) {
        controller.getCustomer(req, res)
    })

    app.delete('/customer/:CUSTOMER_ID',expressJoiMiddleware(schema.deleteCustomerValidation),sessionMiddleware, function (req, res) {
        controller.deleteCustomer(req, res)
    })
}
