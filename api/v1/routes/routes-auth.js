const authController = require('../controllers/controllers-auth')

module.exports = function (app) {
    app.post('/v1/getToken', function (req, res) {
        authController.getToken(req, res)
    })
}
