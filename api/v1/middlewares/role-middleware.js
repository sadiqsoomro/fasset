module.exports = async (req, res, next) => {
    if(res.locals.USER_DATA.IS_ADMIN == 1) next()
    else res.status(401).send({
        "metadata": {
            "status": "4001",
            "description": "Error",
            "responseTime": new Date().toISOString()
        },
        "errorDetails": "You are not authorized to perform this operation."
    })   
}