const jwt = require('jsonwebtoken');
module.exports = async (req, res, next) => {
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) res.status(401).send({
        "metadata": {
            "status": "401",
            "description": "Error",
            "responseTime": new Date().toISOString()
        },
        "errorDetails": "Authorization token not found."
    })
    try {
        const resp = await jwt.verify(token, "aquickbrownfoxjumpsoveralazydog");
        res.locals.USER_DATA = resp.USER_ID;
        next();
    } catch (err) {
        res.status(403).send({
            "metadata": {
                "status": "4003",
                "description": "Error",
                "responseTime": new Date().toISOString()
            },
            "errorDetails": "Invalid token"
        })
    }
}