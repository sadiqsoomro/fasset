const Joi = require('joi')

module.exports.createCustomerValidation = {
    body: {
        NAME: Joi.string().required(),
        EMAIL: Joi.string().required(),
        PHONE_NUM: Joi.string().required()
    }
}

module.exports.getCustomerValidation = {
    query: {
        
    }
}

module.exports.deleteCustomerValidation = {
    params: {
        CUSTOMER_ID : Joi.string().required()
    }
}