const Joi = require('joi')

module.exports.userValidation = {
    body: {
        USER_NAME: Joi.string().alphanum().min(6).max(32).required(),
        PASSWORD:  Joi.string().alphanum().min(6).max(32).required(),
        IS_ADMIN: Joi.number().integer().valid(1, 0).required()
    }
}

module.exports.loginValidation = {
    body: {
        //password complexity is not implemented
        USER_NAME: Joi.string().alphanum().min(6).max(32).required(),
        PASSWORD: Joi.string().alphanum().min(6).max(32).required()
    }
}

module.exports.updateRoleValidation = {
    params: {
        USER_ID: Joi.string().alphanum().min(6).max(32).required(),
        ROLE_TYPE: Joi.number().integer().valid(1, 0).required()
    }
}

module.exports.activateUserValidation = {
    params: {
        USER_ID: Joi.string().alphanum().min(6).max(32).required()
    }
}

//activateUserValidation