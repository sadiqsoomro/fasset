const Sequelize = require("sequelize");

module.exports = () => {
     const model = db.define("FT_USER", {
        UUID: { type: Sequelize.STRING },
        USER_NAME: { type: Sequelize.STRING },
        PASSWORD: { type: Sequelize.STRING },
        IS_ACTIVE: { type: Sequelize.STRING },
        IS_ADMIN: { type: Sequelize.STRING }
    },{
        tableName: 'FT_USER'
    });
    model.removeAttribute('id');
    model.associate = function(models) {
    model.belongsTo(models.FT_CUSTOMER, {foreignKey: 'CREATED_BY',sourceKey: 'UUID'});
    }
    return model;
}