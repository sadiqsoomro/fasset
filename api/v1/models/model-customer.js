const Sequelize = require("sequelize");
const user = require("../models/model-user")();
module.exports = () => {
    const model = db.define("FT_CUSTOMER", {
        UUID: { type: Sequelize.STRING,primaryKey: true},
        NAME: { type: Sequelize.STRING },
        EMAIL: { type: Sequelize.STRING },
        PHONE_NUM: { type: Sequelize.STRING },
        CREATED_BY: { type: Sequelize.STRING }
    },{
        tableName: 'FT_CUSTOMER',
        timestamps: false
    });
    model.removeAttribute('id');
    model.belongsTo(user,{foreignKey: 'CREATED_BY', targetKey: 'UUID'});
    return model;
}