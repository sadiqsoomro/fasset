const Sequelize = require("sequelize");

module.exports = () => {
    const model = db.define("FT_LOGIN", {
        UUID: { type: Sequelize.STRING },
        USER_ID: { type: Sequelize.STRING },
        TOKEN: { type: Sequelize.STRING },
        SESSION_EXPIRY: { type: Sequelize.STRING }
    },{
        tableName: 'FT_LOGIN'
    });
    model.removeAttribute('id');
    return model;

}